FROM ubuntu:18.04
MAINTAINER red <r.nouaf@gmail.com>

# Update repo
# RUN apt-get update && apt-get -y install software-properties-common && add-apt-repository ppa:ondrej/php

# Install Apache
RUN apt-get update && apt-get -y install apache2


# Install PHP modules
# RUN apt-get update && apt-get -y install php7.4 libapache2-mod-php7.4 php7.4-cli php7.4-common php7.4-mbstring php7.4-gd php7.4-intl php7.4-xml php7.4-mysql php7.4-mcrypt php7.4-zip

# copy application files
RUN rm -rf /var/www/htm/*
ADD kiki2 /var/www/html

# Configure apache
RUN chown -R www-data:www-data /var/www
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2

# Open port 80
Expose 80

# Start Apache service
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
